flask==1.1.2
flake8==3.7.9
pytest==5.4.1
coverage==4.5.1
pytest-mock==1.10.4
tox==3.14.6