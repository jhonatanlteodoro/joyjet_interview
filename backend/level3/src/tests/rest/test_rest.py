import json

import pytest


class TestCheckoutViews:

    @pytest.mark.parametrize('data, expected_response, status_code_expected', [
        ({}, ("articles is required", "carts is required",
              "delivery_fees is required", "discounts is required"), 400),
        ({"carts": {"some": "thing"}}, (
            "articles is required", "delivery_fees is required",
            "discounts is required"), 400),
        ({"articles": {"some": "thing"}}, (
            "carts is required", "delivery_fees is required",
            "discounts is required"), 400),
        ({"delivery_fees": {"some": "thing"}}, (
            "carts is required", "articles is required",
            "discounts is required"), 400),
    ])
    def test_checkout_path_params_required(
            self, data, expected_response, status_code_expected, test_client
    ):
        response = test_client.post('/checkout/', json=data)

        assert response.status_code == status_code_expected
        response_data = json.loads(response.data)
        for text_error in expected_response:
            assert text_error in response_data

    def test_checkout_path_success(
            self, test_client, expected_data_entry, expected_data_result
    ):
        response = test_client.post('/checkout/', json=expected_data_entry)
        assert response.status_code == 200
        assert json.loads(response.data) == expected_data_result
