from functools import reduce
import math
import operator

from src import logger


class CheckoutParameter:
    PARAMS_REQUIRED = ('articles', 'carts', 'delivery_fees', "discounts")

    def __init__(self, data):
        self.articles = data.get("articles")
        self.carts = data.get("carts")
        self.delivery_fees = data.get("delivery_fees")
        self.discounts = data.get("discounts")
        self.errors = []

    def to_dict(self):
        return {
            "articles": self.articles,
            "carts": self.carts,
            "delivery_fees": self.delivery_fees,
            "discounts": self.discounts,
        }

    def validate_params_required(self) -> None:
        params = self.to_dict()
        for param in params.items():
            if not param[1] and param[0] in self.PARAMS_REQUIRED:
                self.errors.append(f'{param[0]} is required')

    def validate(self) -> bool:
        self.validate_params_required()
        is_valid = not bool(self.errors)
        logger.debug(
            f'Params valid {is_valid}'
        )
        return is_valid


class CheckoutProcess:

    def __init__(self, parameter: CheckoutParameter):
        self.parameter = parameter

    def get_article_by_id(self, id: int) -> dict:
        filter_by_id = (lambda x: x['id'] == id)
        result = list(filter(filter_by_id, self.parameter.articles))

        if result:
            return result[0]

        return None

    @staticmethod
    def calc_percentage_discount(article, discount, item):
        article_price = article["price"]
        discount_value = discount["value"]
        quantity = item["quantity"]
        unit_price = (article_price - (article_price * discount_value / 100))
        return int(unit_price * quantity)

    @staticmethod
    def calc_amount_discount(article, discount, item):
        article_price = article["price"]
        discount_value = discount["value"]
        unit_price = article_price - discount_value
        return int(unit_price * item["quantity"])

    def get_discount_by_article_id(self, article_id):
        filter_by_id = (lambda x: x['article_id'] == article_id)
        result = list(filter(filter_by_id, self.parameter.discounts))

        if result:
            return result[0]

        return None

    def get_price_list(self, items):
        price_list = []
        for item in items:
            article = self.get_article_by_id(item["article_id"])
            discount = self.get_discount_by_article_id(article["id"])

            if discount:
                if discount["type"] == "amount":
                    price_list.append(
                        self.calc_amount_discount(article, discount, item)
                    )

                elif discount["type"] == "percentage":
                    price_list.append(
                        self.calc_percentage_discount(article, discount, item)
                    )
            else:
                price_list.append(article["price"] * item["quantity"])

        return price_list

    def get_delivery_fee_by_total_price(self, total: int) -> int:
        for fee in self.parameter.delivery_fees:
            range = fee["eligible_transaction_volume"]
            value_min = range.get("min_price")
            value_max = range.get("max_price")
            min_price = value_min if value_min else 0
            max_price = value_max if value_max else math.inf

            if min_price <= total < max_price:
                return fee["price"]

        return 0

    def do_cart_checkout(self) -> dict:
        result_carts = []
        for cart in self.parameter.carts:
            new_data = {"id": cart["id"]}
            items = cart["items"]

            price_list = self.get_price_list(items)

            new_data["total"] = reduce(operator.add, price_list) \
                if price_list else 0

            new_data["total"] += self.get_delivery_fee_by_total_price(
                new_data["total"]
            )

            logger.info(f'Cart processed - ID: {new_data["id"]}')
            logger.debug(f'item: {new_data}')
            result_carts.append(new_data)

        logger.info("All carts are processed")
        logger.debug(f"'carts': {result_carts}")
        return {"carts": result_carts}

    def execute(self) -> dict:
        result = self.do_cart_checkout()
        return result
