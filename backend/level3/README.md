# Level 3

Some products are discounted because of a deal we made with the supplier.

There are two kinds of discounts:
- a direct cut to the article's price, e.g. get 50€ off your 300€ caviar tin and only pay 250€
- a percentage discount, e.g. get 20% off your 5€ creme fraiche and only pay 4€

Write code that generates `output.json` from `data.json`

### How to run script?
- Create a virtualenv env (e.g virtualenvwrapper, pyenv, etc...) using python 3
- Active virtualenv and install requiriments `pip install -r requirements.txt`
- Check if all are ok! run `tox`
- Run server! `python app.py`

## Abount Api
This api has a simple structure, just for test.
We do not find config files for envvars, docker or CI for example, but we have 
a simple configuration of test, using pytest and flake8 as lint.
Look below
```
├── app.py # register your blueprints here and add your configs to run in flask context
├── __init__.py
├── README.md
├── requirements.txt
├── src # put your configs here
│   ├── conftest.py
│   ├── __init__.py
│   ├── process # Do your controllers/process here
│   │   ├── checkout.py
│   │   └── __init__.py
│   ├── rest # Create your views here
│   │   ├── app.py
│   │   ├── checkout
│   │   │   ├── __init__.py
│   │   │   └── views.py
│   │   └── __init__.py
│   └── tests # Do your tests here
│       ├── __init__.py
│       ├── process
│       │   ├── data.json
│       │   ├── output.json
│       │   └── test_process.py
│       └── rest
│           └── test_rest.py
└── tox.ini
```


So, We have just one endpoint available /checkout. How it's work?
The endpoint expected a simple payload to process, and if all are ok, it must be 
response your data processed. look below:

Expected data entry:
```json
{
  "articles": [
    { "id": 1, "name": "water", "price": 100 },
    { "id": 2, "name": "honey", "price": 200 },
    { "id": 3, "name": "mango", "price": 400 },
    { "id": 4, "name": "tea", "price": 1000 },
    { "id": 5, "name": "ketchup", "price": 999 },
    { "id": 6, "name": "mayonnaise", "price": 999 },
    { "id": 7, "name": "fries", "price": 378 },
    { "id": 8, "name": "ham", "price": 147 }
  ],
  "carts": [
    {
      "id": 1,
      "items": [
        { "article_id": 1, "quantity": 6 },
        { "article_id": 2, "quantity": 2 },
        { "article_id": 4, "quantity": 1 }
      ]
    },
    {
      "id": 2,
      "items": [
        { "article_id": 2, "quantity": 1 },
        { "article_id": 3, "quantity": 3 }
      ]
    },
    {
      "id": 3,
      "items": [
        { "article_id": 5, "quantity": 1 },
        { "article_id": 6, "quantity": 1 }
      ]
    },
    {
      "id": 4,
      "items": [
        { "article_id": 7, "quantity": 1 }
      ]
    },
    {
      "id": 5,
      "items": [
        { "article_id": 8, "quantity": 3 }
      ]
    }
  ],
  "delivery_fees": [
    {
      "eligible_transaction_volume": {
        "min_price": 0,
        "max_price": 1000
      },
      "price": 800
    },
    {
      "eligible_transaction_volume": {
        "min_price": 1000,
        "max_price": 2000
      },
      "price": 400
    },
    {
      "eligible_transaction_volume": {
        "min_price": 2000,
        "max_price": null
      },
      "price": 0
    }
  ],
  "discounts": [
    { "article_id": 2, "type": "amount", "value": 25 },
    { "article_id": 5, "type": "percentage", "value": 30 },
    { "article_id": 6, "type": "percentage", "value": 30 },
    { "article_id": 7, "type": "percentage", "value": 25 },
    { "article_id": 8, "type": "percentage", "value": 10 }
  ]
}
```

Expected data reponse:
```json
{
  "carts": [
    {
      "id": 1,
      "total": 2350
    },
    {
      "id": 2,
      "total": 1775
    },
    {
      "id": 3,
      "total": 1798
    },
    {
      "id": 4,
      "total": 1083
    },
    {
      "id": 5,
      "total": 1196
    }
  ]
}
```