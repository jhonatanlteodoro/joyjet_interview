# Level 2

Cost of delivery depends on how much we charged the custormer for their cart’s contents
The more the customer spends, the less they are charged for shipping.

Write code that generates `output.json` from `data.json`

### How to run script?
- Create a virtualenv env (e.g virtualenvwrapper, pyenv, etc...) using python 3
- Active virtualenv and install requiriments `pip install -r requirements.txt`
- Check if all are ok! run `tox`
- Run server! `python app.py`

## Abount Api
This api has a simple structure, just for test.
We do not find config files for envvars, docker or CI for example, but we have 
a simple configuration of test, using pytest and flake8 as lint.
Look below
```
├── app.py # register your blueprints here and add your configs to run in flask context
├── __init__.py
├── README.md
├── requirements.txt
├── src # put your configs here
│   ├── conftest.py
│   ├── __init__.py
│   ├── process # Do your controllers/process here
│   │   ├── checkout.py
│   │   └── __init__.py
│   ├── rest # Create your views here
│   │   ├── app.py
│   │   ├── checkout
│   │   │   ├── __init__.py
│   │   │   └── views.py
│   │   └── __init__.py
│   └── tests # Do your tests here
│       ├── __init__.py
│       ├── process
│       │   ├── data.json
│       │   ├── output.json
│       │   └── test_process.py
│       └── rest
│           └── test_rest.py
└── tox.ini
```


So, We have just one endpoint available /checkout. How it's work?
The endpoint expected a simple payload to process, and if all are ok, it must be 
response your data processed. look below:

Expected data entry:
```json
{
  "articles": [
    { "id": 1, "name": "water", "price": 100 },
    { "id": 2, "name": "honey", "price": 200 },
    { "id": 3, "name": "mango", "price": 400 },
    { "id": 4, "name": "tea", "price": 1000 }
  ],
  "carts": [
    {
      "id": 1,
      "items": [
        { "article_id": 1, "quantity": 6 },
        { "article_id": 2, "quantity": 2 },
        { "article_id": 4, "quantity": 1 }
      ]
    },
    {
      "id": 2,
      "items": [
        { "article_id": 2, "quantity": 1 },
        { "article_id": 3, "quantity": 3 }
      ]
    },
    {
      "id": 3,
      "items": []
    }
  ],
  "delivery_fees": [
    {
      "eligible_transaction_volume": {
        "min_price": 0,
        "max_price": 1000
      },
      "price": 800
    },
    {
      "eligible_transaction_volume": {
        "min_price": 1000,
        "max_price": 2000
      },
      "price": 400
    },
    {
      "eligible_transaction_volume": {
        "min_price": 2000,
        "max_price": null
      },
      "price": 0
    }
  ]
}
```

Expected data reponse:
```json
{
  "carts": [
    {
      "id": 1,
      "total": 2000
    },
    {
      "id": 2,
      "total": 1800
    },
    {
      "id": 3,
      "total": 800
    }
  ]
}
```