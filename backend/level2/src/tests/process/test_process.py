import pytest

from src.process.checkout import CheckoutParameter, CheckoutProcess


class TestCheckoutParameter:

    def test_validate_success(self, expected_data_entry):
        parameter_instance = CheckoutParameter(expected_data_entry)
        assert parameter_instance.validate() is True
        assert len(parameter_instance.errors) == 0

    @pytest.mark.parametrize(
        'data, response_expected', [
            ({}, False),
            ({"carts": {}}, False),
            ({"articles": {}}, False),
            ({"delivery_fees": {}}, False)
        ]
    )
    def test_validate_fail(self, data, response_expected):
        parameter_instance = CheckoutParameter(data)
        assert parameter_instance.validate() is response_expected

    def test_to_dict(self):
        data = {
            "articles": {"some": "thing"},
            "carts": {"some": "thing"},
            "delivery_fees": {"some": "thing"},
        }
        parameter_instance = CheckoutParameter(data)
        result = parameter_instance.to_dict()

        assert isinstance(result, dict)
        assert data["articles"] == result["articles"]
        assert data["carts"] == result["carts"]
        assert data["delivery_fees"] == result["delivery_fees"]


class TestCheckoutProcess:

    @pytest.mark.parametrize('id, expected_data', [
        (1, {"id": 1, "name": "water", "price": 100}),
        (5, None),
        (10, None),
        (3, {"id": 3, "name": "mango", "price": 400})
    ])
    def test_get_article_by_id(self, id, expected_data, expected_data_entry):
        parameter_instance = CheckoutParameter(expected_data_entry)
        process_instance = CheckoutProcess(parameter_instance)

        result = process_instance.get_article_by_id(id)
        assert result == expected_data

    def test_get_price_list(self, expected_data_entry):
        parameter_instance = CheckoutParameter(expected_data_entry)
        process_instance = CheckoutProcess(parameter_instance)

        items = [
            {"article_id": 2, "quantity": 1},
            {"article_id": 3, "quantity": 3}
        ]
        result = process_instance.get_price_list(items)
        must_be_response = [200, 1200]
        assert must_be_response == result

    @pytest.mark.parametrize('total, delivery_fee_expected', [
        (0, 800), (1000, 400), (1500, 400), (2000, 0), (999999, 0)
    ])
    def test_get_delivery_fee_by_total_price(
            self, total, delivery_fee_expected, expected_data_entry
    ):
        parameter_instance = CheckoutParameter(expected_data_entry)
        process_instance = CheckoutProcess(parameter_instance)

        result = process_instance.get_delivery_fee_by_total_price(total)
        assert result == delivery_fee_expected

    def test_do_cart_checkout(self, expected_data_entry, expected_data_result):
        parameter_instance = CheckoutParameter(expected_data_entry)
        process_instance = CheckoutProcess(parameter_instance)

        result = process_instance.do_cart_checkout()

        assert len(result["carts"]) == len(expected_data_result["carts"])
        for idx, content in enumerate(expected_data_result["carts"]):
            assert content == result["carts"][idx]

    def test_execute_call(self, mocker):
        process_instance = CheckoutProcess({"some": "thing"})
        fake_response_do_checkout = {}
        mock_do_checkout = mocker.patch(
            'src.process.checkout.CheckoutProcess.do_cart_checkout',
            return_value=fake_response_do_checkout
        )
        result = process_instance.execute()
        assert mock_do_checkout.called
        assert result == fake_response_do_checkout
