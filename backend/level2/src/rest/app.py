from flask import Flask

from src import logger


def create_app():
    app = Flask(__name__)

    app.logger.addHandler(logger)

    # Do factory - Register blueprint
    from .checkout.views import bp_checkout
    app.register_blueprint(bp_checkout, url_prefix='/checkout')
    return app
