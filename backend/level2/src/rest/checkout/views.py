from flask import Blueprint, jsonify, request

from src.process.checkout import CheckoutProcess, CheckoutParameter

bp_checkout = Blueprint('checkout', __name__)


@bp_checkout.route("/", methods=['POST'])
def get_total():
    data = request.json
    parameter = CheckoutParameter(data)
    if parameter.validate():

        checkout = CheckoutProcess(parameter)
        result = checkout.execute()
        return jsonify(result), 200

    return jsonify(parameter.errors), 400
