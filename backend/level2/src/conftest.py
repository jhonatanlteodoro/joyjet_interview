import json

import pytest

from src.rest.app import create_app


@pytest.fixture
def expected_data_entry():
    with open('src/tests/process/data.json', 'r') as file:
        data = file.read()
    return json.loads(data)


@pytest.fixture
def expected_data_result():
    with open('src/tests/process/output.json', 'r') as file:
        data = file.read()
    return json.loads(data)


@pytest.fixture(scope='module')
def test_client():
    flask_app = create_app()

    testing_client = flask_app.test_client()

    ctx = flask_app.app_context()
    ctx.push()

    yield testing_client

    ctx.pop()
