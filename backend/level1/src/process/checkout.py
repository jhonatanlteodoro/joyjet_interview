from functools import reduce
import operator

from src import logger


class CheckoutParameter:
    PARAMS_REQUIRED = ('articles', 'carts')

    def __init__(self, data):
        self.articles = data.get("articles")
        self.carts = data.get("carts")
        self.errors = []

    def to_dict(self):
        return {
            "articles": self.articles,
            "carts": self.carts
        }

    def validate_params_required(self) -> None:
        params = self.to_dict()
        for param in params.items():
            if not param[1] and param[0] in self.PARAMS_REQUIRED:
                self.errors.append(f'{param[0]} is required')

    def validate(self) -> bool:
        self.validate_params_required()
        is_valid = not bool(self.errors)
        logger.debug(
            f'Params valid {is_valid}'
        )
        return is_valid


class CheckoutProcess:

    def __init__(self, parameter: CheckoutParameter):
        self.parameter = parameter

    def get_article_by_id(self, id: int) -> dict:
        filter_by_id = (lambda x: x['id'] == id)
        result = list(filter(filter_by_id, self.parameter.articles))

        if result:
            return result[0]

        return None

    def get_price_list(self, items):
        price_list = [
            self.get_article_by_id(item["article_id"])["price"] * item[
                "quantity"]
            for item in items
        ]
        return price_list

    def do_cart_checkout(self) -> dict:
        result_carts = []
        for cart in self.parameter.carts:
            new_data = {"id": cart["id"]}
            items = cart["items"]

            price_list = self.get_price_list(items)

            new_data["total"] = reduce(operator.add, price_list) \
                if price_list else 0

            logger.info(f'Cart processed - ID: {new_data["id"]}')
            logger.debug(f'item: {new_data}')
            result_carts.append(new_data)

        logger.info("All carts are processed")
        logger.debug(f"'carts': {result_carts}")
        return {"carts": result_carts}

    def execute(self) -> dict:
        result = self.do_cart_checkout()
        return result
